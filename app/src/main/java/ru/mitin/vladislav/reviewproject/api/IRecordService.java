package ru.mitin.vladislav.reviewproject.api;

import java.util.List;

import io.reactivex.Single;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import ru.mitin.vladislav.reviewproject.api.dto.AppointmentDto;
import ru.mitin.vladislav.reviewproject.api.dto.CrpPatientDto;
import ru.mitin.vladislav.reviewproject.api.dto.DoctorDto;
import ru.mitin.vladislav.reviewproject.api.dto.OrganizationDto;
import ru.mitin.vladislav.reviewproject.api.dto.PostDto;
import ru.mitin.vladislav.reviewproject.api.dto.SessionDto;
import ru.mitin.vladislav.reviewproject.api.dto.SlotDto;

public interface IRecordService {
    @GET("sessions")
    Single<Response<SessionDto>> getSession(@Header("Authorization") String authorization, @Query("source") String source, @Query("author") String author);

    @GET("mo")
    Single<Response<List<OrganizationDto>>> getOrganizations(@Header("Authorization") String authorization, @Header("RIR-Session") String token);

    @POST("mo/{id}/patients")
    Single<Response<CrpPatientDto>> identifyPatient(@Header("Authorization") String authorization, @Header("RIR-Session") String token, @Path("id") long organization, @Body CrpPatientDto patient);

    @GET("mo/{id}/posts")
    Single<Response<List<PostDto>>> getPosts(@Header("Authorization") String authorization, @Header("RIR-Session") String token, @Path("id") long organization);

    @GET("mo/{id}/doctors")
    Single<Response<List<DoctorDto>>> getDoctors(@Header("Authorization") String authorization, @Header("RIR-Session") String token, @Path("id") long organization, @Query("postCode") String postCode);

    @GET("mo/{id}/slots")
    Single<Response<List<SlotDto>>> getSlots(@Header("Authorization") String authorization, @Header("RIR-Session") String token, @Path("id") long organization, @Query("doctorId") String doctorId);

    @POST("mo/{id}/appointmets")
    Single<Response<AppointmentDto>> saveAppointment(@Header("Authorization") String authorization, @Header("RIR-Session") String token, @Path("id") long organization, @Body AppointmentDto appointment);

    @DELETE("mo/{moId}/appointments/{appointmentId}")
    Single<Response<AppointmentDto>> removeAppointment(@Header("Authorization") String authorization, @Header("RIR-Session") String token, @Path("moId") long organization, @Path("appointmentId") long appointment);
}
