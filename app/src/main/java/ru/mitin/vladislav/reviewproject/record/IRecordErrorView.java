package ru.mitin.vladislav.reviewproject.record;

import ru.mitin.vladislav.reviewproject.core.IErrorView;

public interface IRecordErrorView extends IErrorView {
    void setOrganizationError(String message);
    void setPostError(String message);
    void setDoctorError(String message);
}
