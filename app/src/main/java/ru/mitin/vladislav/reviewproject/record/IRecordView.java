package ru.mitin.vladislav.reviewproject.record;

import java.text.SimpleDateFormat;
import java.util.List;

import ru.mitin.vladislav.reviewproject.core.base.IView;
import ru.mitin.vladislav.reviewproject.core.view.IFilterItem;
import ru.mitin.vladislav.reviewproject.model.Doctor;
import ru.mitin.vladislav.reviewproject.model.Organization;
import ru.mitin.vladislav.reviewproject.model.Post;
import ru.mitin.vladislav.reviewproject.model.Slot;

public interface IRecordView extends IView {
    void setOrganizations(List<OrganizationItem> organizations);
    void setPosts(List<PostItem> posts);
    void setDoctors(List<DoctorItem> doctors);
    void setSlots(List<SlotItem> slots);

    class OrganizationItem implements IFilterItem {
        private final long id;
        private String shortName;

        public OrganizationItem(Organization organization) {
            this.id = organization.id;
            this.shortName = organization.shortName;
        }

        @Override
        public String getFilterField() {
            return shortName;
        }

        @Override
        public Object getId() {
            return id;
        }

        @Override
        public String getDisplayField() {
            return shortName;
        }
    }

    class PostItem implements IFilterItem {
        private String code;
        private String name;

        public PostItem(Post post) {
            this.code = post.code;
            this.name = post.name;
        }

        @Override
        public Object getId() {
            return code;
        }

        @Override
        public String getDisplayField() {
            return name;
        }

        @Override
        public String getFilterField() {
            return name;
        }
    }

    class DoctorItem implements IFilterItem {
        private long id;
        private String name;

        public DoctorItem(Doctor doctor) {
            this.id = doctor.id;
            this.name = doctor.lastName + " " + doctor.firstName + " " + doctor.middleName;
        }

        @Override
        public Object getId() {
            return id;
        }

        @Override
        public String getDisplayField() {
            return name;
        }

        @Override
        public String getFilterField() {
            return name;
        }
    }

    class SlotItem implements IFilterItem {
        private String id;
        private String interval;

        public SlotItem(Slot slot) {
            this.id = slot.id;
            this.interval = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss").format(slot.beginDate) + " - " +
                    new SimpleDateFormat("dd.MM.yyyy HH:mm:ss").format(slot.endDate);
        }

        @Override
        public String getFilterField() {
            return interval;
        }

        @Override
        public Object getId() {
            return id;
        }

        @Override
        public String getDisplayField() {
            return interval;
        }
    }
}
