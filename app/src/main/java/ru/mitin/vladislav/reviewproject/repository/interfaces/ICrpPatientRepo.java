package ru.mitin.vladislav.reviewproject.repository.interfaces;

import io.reactivex.Maybe;
import ru.mitin.vladislav.reviewproject.model.CrpPatient;

public interface ICrpPatientRepo {
    Maybe<CrpPatient> identify(long organization);
}
