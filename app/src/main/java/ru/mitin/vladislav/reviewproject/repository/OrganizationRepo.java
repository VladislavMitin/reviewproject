package ru.mitin.vladislav.reviewproject.repository;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Maybe;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;
import retrofit2.Response;
import ru.mitin.vladislav.reviewproject.api.IRecordService;
import ru.mitin.vladislav.reviewproject.api.dto.OrganizationDto;
import ru.mitin.vladislav.reviewproject.core.Constants;
import ru.mitin.vladislav.reviewproject.core.Session;
import ru.mitin.vladislav.reviewproject.model.Organization;
import ru.mitin.vladislav.reviewproject.repository.interfaces.IOrganizationRepo;

public class OrganizationRepo implements IOrganizationRepo {
    private IRecordService service;

    public OrganizationRepo(IRecordService service) {
        this.service = service;
    }

    public Maybe<List<Organization>> get() {
        return service.getOrganizations(Constants.AUTHORIZATION, Session.getInstance().getSession())
                .filter(new Predicate<Response<List<OrganizationDto>>>() {
                    @Override
                    public boolean test(Response<List<OrganizationDto>> listResponse) throws Exception {
                            return listResponse.code() == 200 && listResponse.body() != null;
                        }
                    })
                .map(new Function<Response<List<OrganizationDto>>, List<Organization>>() {
                    @Override
                    public List<Organization> apply(Response<List<OrganizationDto>> listResponse) throws Exception {
                        List<Organization> organizations = new ArrayList<>(listResponse.body().size());

                        for (OrganizationDto organizationDto : listResponse.body()) {
                            organizations.add(Organization.fromDto(organizationDto));
                        }

                        return organizations;
                    }
                });
    }
}
