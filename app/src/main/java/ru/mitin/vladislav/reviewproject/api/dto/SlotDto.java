package ru.mitin.vladislav.reviewproject.api.dto;

public class SlotDto {
    public String id;
    public String beginDate;
    public String endDate;
    public String cabinet;
    public String status;
}
