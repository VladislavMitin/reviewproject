package ru.mitin.vladislav.reviewproject.record;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.ProgressBar;

import java.util.List;

import ru.mitin.vladislav.reviewproject.R;
import ru.mitin.vladislav.reviewproject.core.base.BaseActivity;
import ru.mitin.vladislav.reviewproject.core.view.FilterDropDownView;
import ru.mitin.vladislav.reviewproject.record.validation.LoadDoctorValidator;
import ru.mitin.vladislav.reviewproject.record.validation.PatientIdentificationValidator;

public class RecordActivityView extends BaseActivity<RecordPresenter> implements IRecordView, FilterDropDownView.OnItemSelectedListener, IRecordErrorView {
    private View background;
    private ProgressBar progressBar;
    private FilterDropDownView<OrganizationItem> organizationView;
    private FilterDropDownView<PostItem> postView;
    private FilterDropDownView<DoctorItem> doctorView;
    private FilterDropDownView<SlotItem> slotView;

    public static Intent newIntent(Context context) {
        return new Intent(context, RecordActivityView.class);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_record);

        background = this.findViewById(R.id.background);
        progressBar = this.findViewById(R.id.progress_bar);

        organizationView = this.findViewById(R.id.organization);
        organizationView.addOnItemSelectedListener(this);

        postView = this.findViewById(R.id.post);
        postView.addOnItemSelectedListener(this);

        doctorView = this.findViewById(R.id.doctor);
        doctorView.addOnItemSelectedListener(this);

        slotView = this.findViewById(R.id.slot);
        slotView.addOnItemSelectedListener(this);

        background = this.findViewById(R.id.background);
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.onViewStarted();
    }

    @Override
    protected RecordPresenter getPresenter() {
        return new RecordPresenter(this, getRepoProvider(), getSchedulerProvider());
    }

    @Override
    public void setOrganizations(List<IRecordView.OrganizationItem> organizations) {
        organizationView.setValues(organizations);
    }

    @Override
    public void setPosts(List<IRecordView.PostItem> posts) {
        postView.setValues(posts);
    }

    @Override
    public void setDoctors(List<IRecordView.DoctorItem> doctors) {
        doctorView.setValues(doctors);
    }

    @Override
    public void setSlots(List<IRecordView.SlotItem> slots) {
        slotView.setValues(slots);
    }

    @Override
    public void showMessage(String message) {
        Snackbar.make(background, message, Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void setOrganizationError(String message) {

    }

    @Override
    public void setPostError(String message) {

    }

    @Override
    public void setDoctorError(String message) {

    }

    @Override
    public void clearErrors() {

    }

    @Override
    public void onItemSelected(View view) {
        switch (view.getId()) {
            case R.id.organization:
                postView.clear();
                doctorView.clear();

                presenter.identifyPatient(new PatientIdentificationValidator(RecordActivityView.this, organizationView.getSelectedItem()));
                break;
            case R.id.post:
                doctorView.clear();

                presenter.loadDoctors(
                        new LoadDoctorValidator(
                                RecordActivityView.this,
                                organizationView.getSelectedItem(),
                                postView.getSelectedItem()
                        )
                );
                break;
            case R.id.doctor:
                if(organizationView.getSelectedItem() != null && postView.getSelectedItem() != null
                        && doctorView.getSelectedItem() != null) {
                    presenter.loadSlots(Long.valueOf(organizationView.getSelectedItem().getId().toString()),
                            Long.valueOf(doctorView.getSelectedItem().getId().toString()));
                }

                break;
        }
    }
}
