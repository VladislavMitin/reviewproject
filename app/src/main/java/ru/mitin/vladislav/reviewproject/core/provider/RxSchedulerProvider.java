package ru.mitin.vladislav.reviewproject.core.provider;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import ru.mitin.vladislav.reviewproject.core.provider.interfaces.ISchedulerProvider;

public class RxSchedulerProvider implements ISchedulerProvider {
    @Override
    public Scheduler ioScheduler() {
        return Schedulers.io();
    }

    @Override
    public Scheduler newThreadScheduler() {
        return Schedulers.newThread();
    }

    @Override
    public Scheduler uiScheduler() {
        return AndroidSchedulers.mainThread();
    }

    @Override
    public Scheduler computeScheduler() {
        return Schedulers.computation();
    }
}
