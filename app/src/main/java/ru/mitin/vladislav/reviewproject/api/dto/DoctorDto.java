package ru.mitin.vladislav.reviewproject.api.dto;

import com.google.gson.annotations.SerializedName;

public class DoctorDto {
    @SerializedName("id")
    public Long id;
    @SerializedName("snils")
    public String snils;
    @SerializedName("lastName")
    public String lastName;
    @SerializedName("firstName")
    public String firstName;
    @SerializedName("middle")
    public String middleName;
}
