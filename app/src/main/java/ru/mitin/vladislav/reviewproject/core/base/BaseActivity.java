package ru.mitin.vladislav.reviewproject.core.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import ru.mitin.vladislav.reviewproject.core.App;
import ru.mitin.vladislav.reviewproject.core.provider.interfaces.IRepoProvider;
import ru.mitin.vladislav.reviewproject.core.provider.interfaces.ISchedulerProvider;

public abstract class BaseActivity<P extends BasePresenter> extends AppCompatActivity implements IView {
    protected P presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        presenter = getPresenter();
    }

    protected abstract P getPresenter();

    protected ISchedulerProvider getSchedulerProvider() {
        return ((App) getApplication()).getSchedulerProvider();
    }

    protected IRepoProvider getRepoProvider() {
        return ((App) getApplication()).getRepoProvider();
    }
}
