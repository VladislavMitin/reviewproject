package ru.mitin.vladislav.reviewproject.repository.interfaces;

import java.util.List;

import io.reactivex.Maybe;
import ru.mitin.vladislav.reviewproject.model.Slot;

public interface ISlotRepo {
    Maybe<List<Slot>> get(long organization, long doctor);
}
