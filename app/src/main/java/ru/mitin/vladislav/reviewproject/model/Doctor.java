package ru.mitin.vladislav.reviewproject.model;

import ru.mitin.vladislav.reviewproject.api.dto.DoctorDto;

public class Doctor {
    public Long id;
    public String snils;
    public String lastName;
    public String firstName;
    public String middleName;

    public static Doctor fromDto(DoctorDto dto) {
        Doctor doctor = new Doctor();
        doctor.id = dto.id;
        doctor.snils = dto.snils;
        doctor.lastName = dto.lastName;
        doctor.firstName = dto.firstName;
        doctor.middleName = dto.middleName;
        return doctor;
    }
}
