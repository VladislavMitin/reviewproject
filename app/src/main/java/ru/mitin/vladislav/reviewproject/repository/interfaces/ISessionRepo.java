package ru.mitin.vladislav.reviewproject.repository.interfaces;

import io.reactivex.Maybe;
import ru.mitin.vladislav.reviewproject.core.Session;

public interface ISessionRepo {
    Maybe<Session> get();
}
