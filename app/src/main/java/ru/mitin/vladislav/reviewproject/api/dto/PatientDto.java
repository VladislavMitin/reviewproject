package ru.mitin.vladislav.reviewproject.api.dto;

public class PatientDto {
    public long id;
    public String firstName;
    public String secondName;
    public String middleName;
    public String birthDate;
    public String insurancePolicyNumber;
    public String insuranceOrganization;
    public String organization;
}
