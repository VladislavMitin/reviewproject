package ru.mitin.vladislav.reviewproject.core.view;

public interface IDropDownItem {
    Object getId();
    String getDisplayField();
}
