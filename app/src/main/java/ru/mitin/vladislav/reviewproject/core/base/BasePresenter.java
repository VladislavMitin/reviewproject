package ru.mitin.vladislav.reviewproject.core.base;

import ru.mitin.vladislav.reviewproject.core.IValidator;
import ru.mitin.vladislav.reviewproject.core.NetworkState;
import ru.mitin.vladislav.reviewproject.core.provider.interfaces.ISchedulerProvider;

public abstract class BasePresenter<V extends IView> {
    protected V view;
    protected ISchedulerProvider schedulerProvider;

    protected boolean isRequestAvailable(IValidator validator) {
        if(validator != null && !validator.isValid()) {
            return false;
        } else if(!NetworkState.isConnected(view.getContext())) {
            view.showMessage("Отсутствует подключение к Интернет");
            return false;
        }

        return true;
    }

    public abstract void onViewCreated();
    public abstract void onViewStarted();
    public abstract void onViewStopped();
    public abstract void onViewDestroyed();
}
