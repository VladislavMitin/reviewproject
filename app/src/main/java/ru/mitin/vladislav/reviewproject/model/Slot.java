package ru.mitin.vladislav.reviewproject.model;

import java.text.SimpleDateFormat;
import java.util.Date;

import ru.mitin.vladislav.reviewproject.api.dto.SlotDto;

public class Slot {
    public String id;
    public Date beginDate;
    public Date endDate;
    public String cabinet;
    public boolean free;

    public static Slot fromDto(SlotDto dto) throws Exception {
        Slot slot = new Slot();
        slot.id = dto.id;
        slot.beginDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(dto.beginDate);
        if(dto.endDate != null) slot.endDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(dto.endDate);
        slot.cabinet = dto.cabinet;
        slot.free = dto.status.toLowerCase().equals("free");
        return slot;
    }
}
