package ru.mitin.vladislav.reviewproject.core;

import java.text.SimpleDateFormat;
import java.util.Date;

import ru.mitin.vladislav.reviewproject.api.dto.CrpPatientDto;
import ru.mitin.vladislav.reviewproject.model.CrpPatient;
import ru.mitin.vladislav.reviewproject.model.Patient;

public class User {
    private User() {}

    public long id;
    public String firstName;
    public String secondName;
    public String middleName;
    public Date birthDate;
    public String insurancePolicyNumber;
    public String insuranceOrganization;
    public String organization;
    public CrpPatient crpData;

    private static final Object lock = new Object();

    private static User instance = null;

    public static void set(Patient patient) {
        User user = new User();
        user.id = patient.id;
        user.firstName = patient.firstName;
        user.secondName = patient.secondName;
        user.middleName = patient.middleName;
        user.birthDate = patient.birthDate;
        user.insurancePolicyNumber = patient.insurancePolicyNumber;
        user.insuranceOrganization = patient.insuranceOrganization;
        user.organization = patient.organization;

        synchronized (lock) {
            instance = user;
        }
    }

    public static User getInstance() {
        synchronized (lock) {
            return instance;
        }
    }

    public void setCrpData(CrpPatient crpPatient) {
        synchronized (lock) {
            instance.crpData = crpPatient;
        }
    }

    public String getName() {
        return secondName + " " + firstName + " " + middleName;
    }

    public String getBirthDate() {
        return new SimpleDateFormat("dd.MM.yyyy").format(birthDate);
    }

    public String getInsurancePolicyNumber() {
        return insurancePolicyNumber;
    }

    public String getInsuranceOrganization() {
        return insuranceOrganization;
    }

    public String getOrganization() {
        return organization;
    }

    public CrpPatientDto toCrpPatientDto() {
        CrpPatientDto dto = new CrpPatientDto();
        dto.firstName = this.firstName;
        dto.lastName = this.secondName;
        dto.middleName = this.middleName;
        dto.gender = "М";
        dto.birthDay = new SimpleDateFormat("yyyy-MM-dd").format(birthDate);
        dto.polisNumber = this.insurancePolicyNumber;
        dto.polisSeries = "";

        return dto;
    }
}
