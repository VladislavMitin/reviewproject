package ru.mitin.vladislav.reviewproject.api.dto;

import com.google.gson.annotations.SerializedName;

public class CrpPatientDto {
    @SerializedName("globalPatientId")
    public Long crpId;
    @SerializedName("patientId")
    public Long moId;
    @SerializedName("lastName")
    public String lastName;
    @SerializedName("firstName")
    public String firstName;
    @SerializedName("middle")
    public String middleName;
    @SerializedName("birthday")
    public String birthDay;
    @SerializedName("snils")
    public String snils;
    @SerializedName("gender")
    public String gender;
    @SerializedName("documentN")
    public String documentNumber;
    @SerializedName("documentS")
    public String documentSeries;
    @SerializedName("polisN")
    public String polisNumber;
    @SerializedName("polisS")
    public String polisSeries;
    @SerializedName("phone")
    public String phone;
}
