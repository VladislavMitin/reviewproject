package ru.mitin.vladislav.reviewproject.record.validation;

import ru.mitin.vladislav.reviewproject.core.IValidator;

public interface IPatientIdentificationValidator extends IValidator {
    long getOrganization();
}
