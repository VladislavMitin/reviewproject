package ru.mitin.vladislav.reviewproject.core.base;

import android.content.Context;

public interface IView {
    void showMessage(String message);
    void showProgressBar();
    void hideProgressBar();
    Context getContext();
}
