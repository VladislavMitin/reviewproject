package ru.mitin.vladislav.reviewproject.repository.interfaces;

import java.util.List;

import io.reactivex.Maybe;
import ru.mitin.vladislav.reviewproject.model.Doctor;

public interface IDoctorRepo {
    Maybe<List<Doctor>> get(long organization, String post);
}
