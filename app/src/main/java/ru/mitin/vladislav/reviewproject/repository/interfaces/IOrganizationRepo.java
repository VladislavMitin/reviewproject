package ru.mitin.vladislav.reviewproject.repository.interfaces;

import java.util.List;

import io.reactivex.Maybe;
import ru.mitin.vladislav.reviewproject.model.Organization;

public interface IOrganizationRepo {
    Maybe<List<Organization>> get();
}
