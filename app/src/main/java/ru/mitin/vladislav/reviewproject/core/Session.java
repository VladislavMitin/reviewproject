package ru.mitin.vladislav.reviewproject.core;

public class Session {
    private Session() {}

    private String session;

    private static final Object lockObject = new Object();

    private static Session instance = null;

    public static void setSession(String session) {
        Session s = new Session();
        s.session = session;

        synchronized (lockObject) {
            instance = s;
        }
    }

    public static Session getInstance() {
        synchronized (lockObject) {
            return instance;
        }
    }

    public String getSession() {
        return session;
    }
}
