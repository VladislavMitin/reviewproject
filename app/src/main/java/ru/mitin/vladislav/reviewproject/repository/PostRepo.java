package ru.mitin.vladislav.reviewproject.repository;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Maybe;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;
import retrofit2.Response;
import ru.mitin.vladislav.reviewproject.api.IRecordService;
import ru.mitin.vladislav.reviewproject.api.dto.PostDto;
import ru.mitin.vladislav.reviewproject.core.Constants;
import ru.mitin.vladislav.reviewproject.core.Session;
import ru.mitin.vladislav.reviewproject.model.Post;
import ru.mitin.vladislav.reviewproject.repository.interfaces.IPostRepo;

public class PostRepo implements IPostRepo {
    private IRecordService service;

    public PostRepo(IRecordService service) {
        this.service = service;
    }

    public Maybe<List<Post>> get(long organization) {
        return service.getPosts(Constants.AUTHORIZATION, Session.getInstance().getSession(), organization)
                .filter(new Predicate<Response<List<PostDto>>>() {
                    @Override
                    public boolean test(Response<List<PostDto>> listResponse) throws Exception {
                        return listResponse.code() == 200 && listResponse.body() != null;
                    }
                })
                .map(new Function<Response<List<PostDto>>, List<Post>>() {
                    @Override
                    public List<Post> apply(Response<List<PostDto>> listResponse) throws Exception {
                        List<Post> posts = new ArrayList<>(listResponse.body().size());

                        for (PostDto postDto : listResponse.body()) {
                            posts.add(Post.fromDto(postDto));
                        }

                        return posts;
                    }
                });
    }
}
