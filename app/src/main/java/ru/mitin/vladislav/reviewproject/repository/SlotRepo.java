package ru.mitin.vladislav.reviewproject.repository;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Maybe;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;
import retrofit2.Response;
import ru.mitin.vladislav.reviewproject.api.IRecordService;
import ru.mitin.vladislav.reviewproject.api.dto.SlotDto;
import ru.mitin.vladislav.reviewproject.core.Constants;
import ru.mitin.vladislav.reviewproject.core.Session;
import ru.mitin.vladislav.reviewproject.model.Slot;
import ru.mitin.vladislav.reviewproject.repository.interfaces.ISlotRepo;

public class SlotRepo implements ISlotRepo {
    private IRecordService service;

    public SlotRepo(IRecordService service) {
        this.service = service;
    }

    public Maybe<List<Slot>> get(long organization, long doctor) {
        return service.getSlots(Constants.AUTHORIZATION, Session.getInstance().getSession(), organization, String.valueOf(doctor))
                .filter(new Predicate<Response<List<SlotDto>>>() {
                    @Override
                    public boolean test(Response<List<SlotDto>> listResponse) throws Exception {
                        return listResponse.code() == 200 && listResponse.body() != null;
                    }
                })
                .map(new Function<Response<List<SlotDto>>, List<Slot>>() {
                    @Override
                    public List<Slot> apply(Response<List<SlotDto>> listResponse) throws Exception {
                        List<Slot> slots = new ArrayList<>(listResponse.body().size());

                        for (SlotDto slotDto : listResponse.body()) {
                            slots.add(Slot.fromDto(slotDto));
                        }

                        return slots;
                    }
                });
    }
}
