package ru.mitin.vladislav.reviewproject.repository;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Maybe;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;
import retrofit2.Response;
import ru.mitin.vladislav.reviewproject.api.IRecordService;
import ru.mitin.vladislav.reviewproject.api.dto.DoctorDto;
import ru.mitin.vladislav.reviewproject.core.Constants;
import ru.mitin.vladislav.reviewproject.core.Session;
import ru.mitin.vladislav.reviewproject.model.Doctor;
import ru.mitin.vladislav.reviewproject.repository.interfaces.IDoctorRepo;

public class DoctorRepo implements IDoctorRepo {
    private IRecordService service;

    public DoctorRepo(IRecordService service) {
        this.service = service;
    }

    public Maybe<List<Doctor>> get(long organization, String post) {
        return service.getDoctors(Constants.AUTHORIZATION, Session.getInstance().getSession(), organization, post)
                .filter(new Predicate<Response<List<DoctorDto>>>() {
                    @Override
                    public boolean test(Response<List<DoctorDto>> listResponse) throws Exception {
                        return listResponse.code() == 200 && listResponse.body() != null;
                    }
                })
                .map(new Function<Response<List<DoctorDto>>, List<Doctor>>() {
                    @Override
                    public List<Doctor> apply(Response<List<DoctorDto>> listResponse) throws Exception {
                        List<Doctor> doctors = new ArrayList<>(listResponse.body().size());

                        for (DoctorDto dto : listResponse.body()) {
                            doctors.add(Doctor.fromDto(dto));
                        }

                        return doctors;
                    }
                });
    }
}
