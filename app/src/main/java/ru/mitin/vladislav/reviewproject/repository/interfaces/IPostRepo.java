package ru.mitin.vladislav.reviewproject.repository.interfaces;

import java.util.List;

import io.reactivex.Maybe;
import ru.mitin.vladislav.reviewproject.model.Post;

public interface IPostRepo {
    Maybe<List<Post>> get(long organization);
}
