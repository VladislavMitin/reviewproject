package ru.mitin.vladislav.reviewproject.record.validation;

import ru.mitin.vladislav.reviewproject.core.view.IDropDownItem;
import ru.mitin.vladislav.reviewproject.record.IRecordErrorView;

public class PatientIdentificationValidator implements IPatientIdentificationValidator {
    private IRecordErrorView view;

    private IDropDownItem organization;

    public PatientIdentificationValidator(IRecordErrorView view, IDropDownItem organization) {
        this.view = view;
        this.organization = organization;
    }

    @Override
    public long getOrganization() {
        return Long.parseLong(organization.getId().toString());
    }

    @Override
    public boolean isValid() {
        boolean isValid = true;
        view.clearErrors();

        if(organization == null) {
            isValid = false;
            view.setOrganizationError("Необходимо выбрать организацию");
        } else if(organization.getId() == null || organization.getDisplayField() == null) {
            isValid = false;
            view.setOrganizationError("Необходимо выбрать организацию");
        } else if(organization.getId().toString().equals(organization.getDisplayField())) {
            isValid = false;
            view.setOrganizationError("Необходимо выбрать организацию");
        } else {
            try {
                Long.parseLong(organization.getId().toString());
            } catch (NumberFormatException ex) {
                isValid = false;
                view.setOrganizationError("Необходимо выбрать организацию");
            }
        }

        return isValid;
    }
}
