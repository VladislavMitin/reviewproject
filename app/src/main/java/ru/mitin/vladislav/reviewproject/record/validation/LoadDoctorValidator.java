package ru.mitin.vladislav.reviewproject.record.validation;

import ru.mitin.vladislav.reviewproject.core.view.IDropDownItem;
import ru.mitin.vladislav.reviewproject.record.IRecordErrorView;

public class LoadDoctorValidator implements ILoadDoctorValidator {
    private IRecordErrorView view;

    private IDropDownItem organization;
    private IDropDownItem post;

    public LoadDoctorValidator(IRecordErrorView view, IDropDownItem organization, IDropDownItem post) {
        this.view = view;
        this.organization = organization;
        this.post = post;
    }

    @Override
    public long getOrganization() {
        return Long.parseLong(organization.getId().toString());
    }

    @Override
    public String getPostCode() {
        return post.getId().toString();
    }

    @Override
    public boolean isValid() {
        boolean isValid = true;
        view.clearErrors();

        if(organization == null) {
            isValid = false;
            view.setOrganizationError("Необходимо выбрать организацию");
        } else if(organization.getId() == null || organization.getDisplayField() == null) {
            isValid = false;
            view.setOrganizationError("Необходимо выбрать организацию");
        } else if(organization.getId().toString().equals(organization.getDisplayField())) {
            isValid = false;
            view.setOrganizationError("Необходимо выбрать организацию");
        } else {
            try {
                Long.parseLong(organization.getId().toString());
            } catch (NumberFormatException ex) {
                isValid = false;
                view.setOrganizationError("Необходимо выбрать организацию");
            }
        }

        if(post == null) {
            isValid = false;
            view.setPostError("Необходимо выбрать должность");
        } else if(post.getId() == null || post.getDisplayField() == null) {
            isValid = false;
            view.setPostError("Необходимо выбрать должность");
        } else if(post.getId().toString().equals(post.getDisplayField())) {
            isValid = false;
            view.setPostError("Необходимо выбрать должность");
        }

        return isValid;
    }
}
