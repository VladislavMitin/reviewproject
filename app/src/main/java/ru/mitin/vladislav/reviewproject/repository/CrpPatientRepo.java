package ru.mitin.vladislav.reviewproject.repository;

import io.reactivex.Maybe;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;
import retrofit2.Response;
import ru.mitin.vladislav.reviewproject.api.IRecordService;
import ru.mitin.vladislav.reviewproject.api.dto.CrpPatientDto;
import ru.mitin.vladislav.reviewproject.core.Constants;
import ru.mitin.vladislav.reviewproject.core.Session;
import ru.mitin.vladislav.reviewproject.core.User;
import ru.mitin.vladislav.reviewproject.model.CrpPatient;
import ru.mitin.vladislav.reviewproject.repository.interfaces.ICrpPatientRepo;

public class CrpPatientRepo implements ICrpPatientRepo {
    private IRecordService service;

    public CrpPatientRepo(IRecordService service) {
        this.service = service;
    }

    public Maybe<CrpPatient> identify(long organization) {
        return service.identifyPatient(Constants.AUTHORIZATION, Session.getInstance().getSession(), organization, User.getInstance().toCrpPatientDto())
                .filter(new Predicate<Response<CrpPatientDto>>() {
                    @Override
                    public boolean test(Response<CrpPatientDto> listResponse) throws Exception {
                        return listResponse.code() == 200;
                    }
                })
                .map(new Function<Response<CrpPatientDto>, CrpPatient>() {
                    @Override
                    public CrpPatient apply(Response<CrpPatientDto> crpPatientDtoResponse) throws Exception {
                        CrpPatient patient = null;

                        if(crpPatientDtoResponse.body() != null) {
                            patient = CrpPatient.fromDto(crpPatientDtoResponse.body());

                            User.getInstance().setCrpData(patient);
                        }

                        return patient;
                    }
                });
    }
}
