package ru.mitin.vladislav.reviewproject.core.provider;

import ru.mitin.vladislav.reviewproject.api.IRecordService;
import ru.mitin.vladislav.reviewproject.core.RetrofitWrapper;
import ru.mitin.vladislav.reviewproject.core.provider.interfaces.IRepoProvider;
import ru.mitin.vladislav.reviewproject.repository.CrpPatientRepo;
import ru.mitin.vladislav.reviewproject.repository.DoctorRepo;
import ru.mitin.vladislav.reviewproject.repository.OrganizationRepo;
import ru.mitin.vladislav.reviewproject.repository.PostRepo;
import ru.mitin.vladislav.reviewproject.repository.SessionRepo;
import ru.mitin.vladislav.reviewproject.repository.SlotRepo;
import ru.mitin.vladislav.reviewproject.repository.interfaces.ICrpPatientRepo;
import ru.mitin.vladislav.reviewproject.repository.interfaces.IDoctorRepo;
import ru.mitin.vladislav.reviewproject.repository.interfaces.IOrganizationRepo;
import ru.mitin.vladislav.reviewproject.repository.interfaces.IPostRepo;
import ru.mitin.vladislav.reviewproject.repository.interfaces.ISessionRepo;
import ru.mitin.vladislav.reviewproject.repository.interfaces.ISlotRepo;

public class RepoProvider implements IRepoProvider {
    @Override
    public ICrpPatientRepo getCrpPatientRepo() {
        return new CrpPatientRepo(RetrofitWrapper.getService(IRecordService.class));
    }

    @Override
    public IDoctorRepo getDoctorRepo() {
        return new DoctorRepo(RetrofitWrapper.getService(IRecordService.class));
    }

    @Override
    public IOrganizationRepo getOrganizationRepo() {
        return new OrganizationRepo(RetrofitWrapper.getService(IRecordService.class));
    }

    @Override
    public IPostRepo getPostRepo() {
        return new PostRepo(RetrofitWrapper.getService(IRecordService.class));
    }

    @Override
    public ISessionRepo getSessionRepo() {
        return new SessionRepo(RetrofitWrapper.getService(IRecordService.class));
    }

    @Override
    public ISlotRepo getSlotRepo() {
        return new SlotRepo(RetrofitWrapper.getService(IRecordService.class));
    }
}
