package ru.mitin.vladislav.reviewproject.api.dto;

public class OrganizationDto {
    public Long id;
    public String oid;
    public String name;
    public String shortName;
    public String address;
    public String cityFiasAoid;
    public String cityFiasAoguid;
    public String phone;
}
