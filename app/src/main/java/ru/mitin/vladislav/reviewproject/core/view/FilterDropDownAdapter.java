package ru.mitin.vladislav.reviewproject.core.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import ru.mitin.vladislav.reviewproject.R;

public class FilterDropDownAdapter extends ArrayAdapter<IDropDownItem> {
    private Context context;
    private List<IDropDownItem> values;

    public FilterDropDownAdapter(@NonNull Context context, int resource, List<IDropDownItem> values) {
        super(context, resource);
        this.context = context;
        this.values = values;
    }

    @Override
    public int getCount() {
        return values != null ? values.size() : 0;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return handleView(position, convertView, parent);
    }

    private View handleView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ViewHolder viewHolder = null;
        View view = convertView;

        if(view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.filter_drop_down_item, parent, false);
            viewHolder = new ViewHolder(view);
        } else {
            viewHolder = ((ViewHolder) view.getTag());
        }

        viewHolder.bind(values.get(position));

        return view;
    }

    @Override
    public void add(@Nullable IDropDownItem item) {
        values.add(item);
    }

    @Override
    public void clear() {
        values.clear();
    }

    public IDropDownItem getItem(int position) {
        return (values != null && values.size() > position) ? values.get(position) : null;
    }

    private class ViewHolder {
        private Object id;
        private TextView textView;

        public ViewHolder(View view) {
            textView = view.findViewById(R.id.display_value);

            view.setTag(this);
        }

        public void bind(IDropDownItem item) {
            this.id = item.getId();
            this.textView.setText(item.getDisplayField());
        }
    }
}
