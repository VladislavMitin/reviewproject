package ru.mitin.vladislav.reviewproject.repository;

import io.reactivex.Maybe;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;
import retrofit2.Response;
import ru.mitin.vladislav.reviewproject.api.IRecordService;
import ru.mitin.vladislav.reviewproject.api.dto.SessionDto;
import ru.mitin.vladislav.reviewproject.core.Constants;
import ru.mitin.vladislav.reviewproject.core.Session;
import ru.mitin.vladislav.reviewproject.core.User;
import ru.mitin.vladislav.reviewproject.repository.interfaces.ISessionRepo;

public class SessionRepo implements ISessionRepo {
    private IRecordService service;

    public SessionRepo(IRecordService service) {
        this.service = service;
    }

    public Maybe<Session> get() {
        return service.getSession(Constants.AUTHORIZATION, Constants.SOURCE, User.getInstance().getName())
                .filter(new Predicate<Response<SessionDto>>() {
                    @Override
                    public boolean test(Response<SessionDto> sessionDtoResponse) throws Exception {
                        return sessionDtoResponse.code() == 200 && sessionDtoResponse.body() != null;
                    }
                })
                .map(new Function<Response<SessionDto>, Session>() {
                    @Override
                    public Session apply(Response<SessionDto> sessionDtoResponse) throws Exception {
                        Session.setSession(sessionDtoResponse.body().session);
                        return Session.getInstance();
                    }
                });
    }
}
