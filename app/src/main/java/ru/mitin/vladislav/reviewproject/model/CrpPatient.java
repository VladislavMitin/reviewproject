package ru.mitin.vladislav.reviewproject.model;

import ru.mitin.vladislav.reviewproject.api.dto.CrpPatientDto;

public class CrpPatient {
    public Long globalId;
    public Long id;

    public static CrpPatient fromDto(CrpPatientDto dto) {
        CrpPatient patient = new CrpPatient();
        patient.globalId = dto.crpId;
        patient.id = dto.moId;

        return patient;
    }
}
