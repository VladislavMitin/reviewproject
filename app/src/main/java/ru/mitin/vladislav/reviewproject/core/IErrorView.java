package ru.mitin.vladislav.reviewproject.core;

public interface IErrorView {
    void clearErrors();
}
