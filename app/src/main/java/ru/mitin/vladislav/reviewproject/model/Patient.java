package ru.mitin.vladislav.reviewproject.model;

import java.text.SimpleDateFormat;
import java.util.Date;

import ru.mitin.vladislav.reviewproject.api.dto.PatientDto;

public class Patient {
    public long id;
    public String firstName;
    public String secondName;
    public String middleName;
    public Date birthDate;
    public String insurancePolicyNumber;
    public String insuranceOrganization;
    public String organization;

    public static Patient fromDto(PatientDto dto) throws Exception {
        Patient patient = new Patient();
        patient.id = dto.id;
        patient.firstName = dto.firstName;
        patient.secondName = dto.secondName;
        patient.middleName = dto.middleName;
        patient.birthDate = new SimpleDateFormat("dd.MM.yyyy").parse(dto.birthDate);
        patient.insurancePolicyNumber = dto.insurancePolicyNumber;
        patient.insuranceOrganization = dto.insuranceOrganization;
        patient.organization = dto.organization;
        return patient;
    }
}
