package ru.mitin.vladislav.reviewproject.core;

public interface IValidator {
    boolean isValid();
}
