package ru.mitin.vladislav.reviewproject.core;

import android.app.Application;

import ru.mitin.vladislav.reviewproject.core.provider.RepoProvider;
import ru.mitin.vladislav.reviewproject.core.provider.RxSchedulerProvider;
import ru.mitin.vladislav.reviewproject.core.provider.interfaces.IRepoProvider;
import ru.mitin.vladislav.reviewproject.core.provider.interfaces.ISchedulerProvider;

public class App extends Application {
    private ISchedulerProvider schedulerProvider = new RxSchedulerProvider();
    private IRepoProvider repoProvider = new RepoProvider();

    public ISchedulerProvider getSchedulerProvider() {
        return schedulerProvider;
    }

    public IRepoProvider getRepoProvider() {
        return repoProvider;
    }
}
