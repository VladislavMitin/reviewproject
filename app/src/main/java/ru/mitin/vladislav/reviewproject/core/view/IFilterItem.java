package ru.mitin.vladislav.reviewproject.core.view;

public interface IFilterItem extends IDropDownItem {
    String getFilterField();
}
