package ru.mitin.vladislav.reviewproject.model;

import ru.mitin.vladislav.reviewproject.api.dto.PostDto;

public class Post {
    public String code;
    public String name;

    public static Post fromDto(PostDto dto) {
        Post post = new Post();
        post.code = dto.code;
        post.name = dto.name;
        return post;
    }
}
