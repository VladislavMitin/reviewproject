package ru.mitin.vladislav.reviewproject.model;

import ru.mitin.vladislav.reviewproject.api.dto.OrganizationDto;

public class Organization {
    public Long id;
    public String oid;
    public String name;
    public String shortName;
    public String address;
    public String cityFiasAoid;
    public String cityFiasAoguid;
    public String phone;

    public static Organization fromDto(OrganizationDto dto) {
        Organization organization = new Organization();
        organization.id = dto.id;
        organization.oid = dto.oid;
        organization.name = dto.name;
        organization.shortName = dto.shortName;
        organization.address = dto.address;
        organization.cityFiasAoid = dto.cityFiasAoid;
        organization.cityFiasAoguid = dto.cityFiasAoguid;
        organization.phone = dto.phone;

        return organization;
    }
}
