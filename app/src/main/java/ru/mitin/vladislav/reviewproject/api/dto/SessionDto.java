package ru.mitin.vladislav.reviewproject.api.dto;

import com.google.gson.annotations.SerializedName;

public class SessionDto {
    @SerializedName("sessionId")
    public String session;
}
