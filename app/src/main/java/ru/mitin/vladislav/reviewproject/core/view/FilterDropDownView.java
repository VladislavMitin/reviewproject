package ru.mitin.vladislav.reviewproject.core.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.ListPopupWindow;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;

import java.util.ArrayList;
import java.util.List;

import ru.mitin.vladislav.reviewproject.R;

public class FilterDropDownView<T extends IFilterItem> extends AppCompatEditText implements View.OnClickListener, AdapterView.OnItemClickListener, View.OnFocusChangeListener {
    private List<T> values = null;

    private ListPopupWindow popupWindow;
    private FilterDropDownAdapter adapter;

    private int popupItemHeight = ListPopupWindow.WRAP_CONTENT;
    private int popupMaxItemCount = 100;

    private IDropDownItem selectedItem = null;

    private IDropDownItem emptyItem;

    private List<OnItemSelectedListener> onItemSelectedListeners;

    public IDropDownItem getSelectedItem() {
        return selectedItem;
    }

    public void addOnItemSelectedListener(OnItemSelectedListener listener) {
        this.onItemSelectedListeners.add(listener);
    }

    public void removeOnItemSelectedListener(OnItemSelectedListener listener) {
        this.onItemSelectedListeners.remove(listener);
    }

    public FilterDropDownView(Context context) {
        super(context);
        init(context);
    }

    public FilterDropDownView(Context context, AttributeSet attrs) {
        super(context, attrs);
        handleAttributes(context, attrs);
        init(context);
    }

    public FilterDropDownView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        handleAttributes(context, attrs);
        init(context);
    }

    private void handleAttributes(Context context, AttributeSet attrs) {
        TypedArray arr = context.obtainStyledAttributes(attrs, R.styleable.FilterDropDownView);

        int itemHeight = arr.getDimensionPixelSize(R.styleable.FilterDropDownView_popupItemHeight, 0);

        if (itemHeight != 0) {
            popupItemHeight = itemHeight;
        }

        int itemCount = arr.getInteger(R.styleable.FilterDropDownView_popupMaxItemCount, 0);

        if(itemCount != 0) {
            popupMaxItemCount = itemCount;
        }

        arr.recycle();
    }

    private void init(Context context) {
        emptyItem = new EmptyItem("Список пуст");

        onItemSelectedListeners = new ArrayList<>();

        adapter = new FilterDropDownAdapter(context, R.layout.filter_drop_down_item, new ArrayList<IDropDownItem>());
        adapter.add(emptyItem);

        popupWindow = new ListPopupWindow(context);
        popupWindow.setAnchorView(this);
        popupWindow.setAdapter(adapter);
        popupWindow.setOnItemClickListener(this);

        if(adapter.getCount() < popupMaxItemCount) {
            popupWindow.setHeight(ListPopupWindow.WRAP_CONTENT);
        } else {
            popupWindow.setHeight(popupItemHeight * popupMaxItemCount);
        }

        this.setOnFocusChangeListener(this);
        this.setOnClickListener(this);
        this.addTextChangedListener(textWatcher);
    }

    private void notifyItemSelected() {
        for (OnItemSelectedListener listener : onItemSelectedListeners) {
            listener.onItemSelected(this);
        }
    }

    public void setValues(List<T> values) {
        this.values = values;

        adapter.clear();

        if(values != null) {
            for (T value : values) {
                adapter.add(value);
            }
        } else {
            adapter.add(emptyItem);
        }

        if(adapter.getCount() < popupMaxItemCount) {
            popupWindow.setHeight(ListPopupWindow.WRAP_CONTENT);
        } else {
            popupWindow.setHeight(popupItemHeight * popupMaxItemCount);
        }

        adapter.notifyDataSetChanged();
    }

    public void clear() {
        setValues(null);
        setText(null);
    }

    @Override
    public void onClick(View v) {
        if(isFocused() && !popupWindow.isShowing()) {
            popupWindow.show();
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        setSelectedItem(adapter.getItem(position));
    }

    private void setSelectedItem(IDropDownItem item) {
        if(item.getId() != null) {
            this.removeTextChangedListener(textWatcher);

            this.selectedItem = item;
            this.setText(item.getDisplayField());
            this.setSelection(this.getText().length());

            popupWindow.dismiss();

            notifyItemSelected();

            this.addTextChangedListener(textWatcher);
        }
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if(this.isAttachedToWindow() && hasFocus && !popupWindow.isShowing()) {
            popupWindow.show();
        } else if(this.isAttachedToWindow() && !hasFocus && popupWindow.isShowing()) {
            popupWindow.dismiss();
        }
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();

        if(hasFocus() && !popupWindow.isShowing()) {
            popupWindow.show();
        }
    }

    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            selectedItem = null;

            List<T> filteredValues = new ArrayList<>();

            String searchText = s.toString();

            if(values != null) {
                for (T value : values) {
                    if (value.getFilterField().toLowerCase().contains(searchText.toLowerCase())) {
                        filteredValues.add(value);
                    }
                }
            }

            adapter.clear();

            if(filteredValues.size() > 0) {
                if(filteredValues.size() == 1 && filteredValues.get(0).getFilterField().equals(searchText)) {
                    setSelectedItem(filteredValues.get(0));
                    return;
                }

                for (T value : filteredValues) {
                    adapter.add(value);
                }
            } else {
                adapter.add(emptyItem);
            }

            if(adapter.getCount() < popupMaxItemCount) {
                popupWindow.setHeight(ListPopupWindow.WRAP_CONTENT);
            } else {
                popupWindow.setHeight(popupItemHeight * popupMaxItemCount);
            }

            adapter.notifyDataSetChanged();

            notifyItemSelected();

            if(isFocused() && !popupWindow.isShowing()) {
                popupWindow.show();
            }
        }
    };

    private class EmptyItem implements IDropDownItem {
        private String displayText;

        public EmptyItem(String displayText) {
            this.displayText = displayText;
        }

        @Override
        public Object getId() {
            return null;
        }

        @Override
        public String getDisplayField() {
            return displayText;
        }
    }

    public interface OnItemSelectedListener {
        void onItemSelected(View view);
    }
}
