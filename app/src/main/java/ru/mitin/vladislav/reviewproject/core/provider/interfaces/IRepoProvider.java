package ru.mitin.vladislav.reviewproject.core.provider.interfaces;

import ru.mitin.vladislav.reviewproject.repository.interfaces.ICrpPatientRepo;
import ru.mitin.vladislav.reviewproject.repository.interfaces.IDoctorRepo;
import ru.mitin.vladislav.reviewproject.repository.interfaces.IOrganizationRepo;
import ru.mitin.vladislav.reviewproject.repository.interfaces.IPostRepo;
import ru.mitin.vladislav.reviewproject.repository.interfaces.ISessionRepo;
import ru.mitin.vladislav.reviewproject.repository.interfaces.ISlotRepo;

public interface IRepoProvider {
    ICrpPatientRepo getCrpPatientRepo();
    IDoctorRepo getDoctorRepo();
    IOrganizationRepo getOrganizationRepo();
    IPostRepo getPostRepo();
    ISessionRepo getSessionRepo();
    ISlotRepo getSlotRepo();
}
