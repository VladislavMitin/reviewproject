package ru.mitin.vladislav.reviewproject.record.validation;

import ru.mitin.vladislav.reviewproject.core.IValidator;

public interface ILoadDoctorValidator extends IValidator {
    long getOrganization();
    String getPostCode();
}
