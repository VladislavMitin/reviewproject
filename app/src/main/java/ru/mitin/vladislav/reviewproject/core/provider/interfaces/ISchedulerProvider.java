package ru.mitin.vladislav.reviewproject.core.provider.interfaces;

import io.reactivex.Scheduler;

public interface ISchedulerProvider {
    Scheduler ioScheduler();
    Scheduler newThreadScheduler();
    Scheduler uiScheduler();
    Scheduler computeScheduler();
}
