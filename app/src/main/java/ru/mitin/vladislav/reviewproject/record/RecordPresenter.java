package ru.mitin.vladislav.reviewproject.record;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.MaybeObserver;
import io.reactivex.MaybeSource;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Function;
import ru.mitin.vladislav.reviewproject.R;
import ru.mitin.vladislav.reviewproject.core.Session;
import ru.mitin.vladislav.reviewproject.core.base.BasePresenter;
import ru.mitin.vladislav.reviewproject.core.provider.interfaces.IRepoProvider;
import ru.mitin.vladislav.reviewproject.core.provider.interfaces.ISchedulerProvider;
import ru.mitin.vladislav.reviewproject.model.CrpPatient;
import ru.mitin.vladislav.reviewproject.model.Doctor;
import ru.mitin.vladislav.reviewproject.model.Organization;
import ru.mitin.vladislav.reviewproject.model.Post;
import ru.mitin.vladislav.reviewproject.model.Slot;
import ru.mitin.vladislav.reviewproject.record.validation.ILoadDoctorValidator;
import ru.mitin.vladislav.reviewproject.record.validation.IPatientIdentificationValidator;
import ru.mitin.vladislav.reviewproject.repository.interfaces.ICrpPatientRepo;
import ru.mitin.vladislav.reviewproject.repository.interfaces.IDoctorRepo;
import ru.mitin.vladislav.reviewproject.repository.interfaces.IOrganizationRepo;
import ru.mitin.vladislav.reviewproject.repository.interfaces.IPostRepo;
import ru.mitin.vladislav.reviewproject.repository.interfaces.ISessionRepo;
import ru.mitin.vladislav.reviewproject.repository.interfaces.ISlotRepo;

public class RecordPresenter extends BasePresenter<IRecordView> {
    private ISessionRepo sessionRepo;
    private IOrganizationRepo organizationRepo;
    private ICrpPatientRepo crpPatientRepo;
    private IPostRepo postRepo;
    private IDoctorRepo doctorRepo;
    private ISlotRepo slotRepo;

    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public RecordPresenter(IRecordView view, IRepoProvider repoProvider, ISchedulerProvider schedulerProvider) {
        this.view = view;
        this.sessionRepo = repoProvider.getSessionRepo();
        this.organizationRepo = repoProvider.getOrganizationRepo();
        this.crpPatientRepo = repoProvider.getCrpPatientRepo();
        this.postRepo = repoProvider.getPostRepo();
        this.doctorRepo = repoProvider.getDoctorRepo();
        this.slotRepo = repoProvider.getSlotRepo();
        this.schedulerProvider = schedulerProvider;
    }

    private void loadOrganization() {
        if(isRequestAvailable(null)) {
            sessionRepo.get()
                    .flatMap(new Function<Session, MaybeSource<List<Organization>>>() {
                        @Override
                        public MaybeSource<List<Organization>> apply(Session session) throws Exception {
                            return organizationRepo.get();
                        }
                    })
                    .subscribeOn(schedulerProvider.ioScheduler())
                    .observeOn(schedulerProvider.uiScheduler())
                    .doFinally(new Action() {
                        @Override
                        public void run() throws Exception {
                            view.hideProgressBar();
                        }
                    })
                    .subscribe(new MaybeObserver<List<Organization>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            compositeDisposable.add(d);
                            view.showProgressBar();
                        }

                        @Override
                        public void onSuccess(List<Organization> organizations) {
                            List<IRecordView.OrganizationItem> items = new ArrayList<>(organizations.size());

                            for (Organization organization : organizations) {
                                items.add(new IRecordView.OrganizationItem(organization));
                            }

                            view.setOrganizations(items);
                        }

                        @Override
                        public void onError(Throwable e) {
                            view.showMessage(view.getContext().getResources().getString(R.string.request_error));
                        }

                        @Override
                        public void onComplete() {
                            view.showMessage(view.getContext().getResources().getString(R.string.request_error));
                        }
                    });
        }
    }

    public void identifyPatient(final IPatientIdentificationValidator validator) {
        if(isRequestAvailable(validator)) {
            crpPatientRepo.identify(validator.getOrganization())
                    .subscribeOn(schedulerProvider.ioScheduler())
                    .observeOn(schedulerProvider.uiScheduler())
                    .doFinally(new Action() {
                        @Override
                        public void run() throws Exception {
                            view.hideProgressBar();
                        }
                    })
                    .subscribe(new MaybeObserver<CrpPatient>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            compositeDisposable.add(d);
                            view.showProgressBar();
                        }

                        @Override
                        public void onSuccess(CrpPatient patient) {
                            if (patient != null) {
                                loadPosts(validator.getOrganization());
                            } else {
                                view.showMessage(view.getContext().getResources().getString(R.string.patient_identify_error));
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            view.showMessage(view.getContext().getResources().getString(R.string.request_error));
                        }

                        @Override
                        public void onComplete() {
                            view.showMessage(view.getContext().getResources().getString(R.string.request_error));
                        }
                    });
        }
    }

    private void loadPosts(long organization) {
        if(isRequestAvailable(null)) {
            postRepo.get(organization)
                    .subscribeOn(schedulerProvider.ioScheduler())
                    .observeOn(schedulerProvider.uiScheduler())
                    .doFinally(new Action() {
                        @Override
                        public void run() throws Exception {
                            view.hideProgressBar();
                        }
                    })
                    .subscribe(new MaybeObserver<List<Post>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            compositeDisposable.add(d);
                            view.showProgressBar();
                        }

                        @Override
                        public void onSuccess(List<Post> posts) {
                            List<IRecordView.PostItem> items = new ArrayList<>(posts.size());

                            for (Post post : posts) {
                                items.add(new IRecordView.PostItem(post));
                            }

                            view.setPosts(items);
                        }

                        @Override
                        public void onError(Throwable e) {
                            view.showMessage(view.getContext().getResources().getString(R.string.request_error));
                        }

                        @Override
                        public void onComplete() {
                            view.showMessage(view.getContext().getResources().getString(R.string.request_error));
                        }
                    });
        }
    }

    public void loadDoctors(ILoadDoctorValidator validator) {
        if(isRequestAvailable(validator)) {
            doctorRepo.get(validator.getOrganization(), validator.getPostCode())
                    .subscribeOn(schedulerProvider.ioScheduler())
                    .observeOn(schedulerProvider.uiScheduler())
                    .doFinally(new Action() {
                        @Override
                        public void run() throws Exception {
                            view.hideProgressBar();
                        }
                    })
                    .subscribe(new MaybeObserver<List<Doctor>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            compositeDisposable.add(d);
                            view.showProgressBar();
                        }

                        @Override
                        public void onSuccess(List<Doctor> doctors) {
                            List<IRecordView.DoctorItem> items = new ArrayList<>(doctors.size());

                            for (Doctor doctor : doctors) {
                                items.add(new IRecordView.DoctorItem(doctor));
                            }

                            view.setDoctors(items);
                        }

                        @Override
                        public void onError(Throwable e) {
                            view.showMessage(view.getContext().getResources().getString(R.string.request_error));
                        }

                        @Override
                        public void onComplete() {
                            view.showMessage(view.getContext().getResources().getString(R.string.request_error));
                        }
                    });
        }
    }

    public void loadSlots(long organization, long doctor) {
        slotRepo.get(organization, doctor)
                .subscribeOn(schedulerProvider.ioScheduler())
                .observeOn(schedulerProvider.uiScheduler())
                .doFinally(new Action() {
                    @Override
                    public void run() throws Exception {
                        view.hideProgressBar();
                    }
                })
                .subscribe(new MaybeObserver<List<Slot>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                        view.showProgressBar();
                    }

                    @Override
                    public void onSuccess(List<Slot> slots) {
                        List<IRecordView.SlotItem> items = new ArrayList<>();

                        for (Slot slot : slots) {
                            if(slot.free) {
                                items.add(new IRecordView.SlotItem(slot));
                            }
                        }

                        view.setSlots(items.size() != 0 ? items : null);
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.showMessage(view.getContext().getResources().getString(R.string.request_error));
                    }

                    @Override
                    public void onComplete() {
                        view.showMessage(view.getContext().getResources().getString(R.string.request_error));
                    }
                });
    }

    @Override
    public void onViewCreated() {

    }

    @Override
    public void onViewStarted() {
        loadOrganization();
    }

    @Override
    public void onViewStopped() {
        compositeDisposable.clear();
    }

    @Override
    public void onViewDestroyed() {
        compositeDisposable.dispose();
    }
}
