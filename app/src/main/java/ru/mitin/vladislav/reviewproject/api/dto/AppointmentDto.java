package ru.mitin.vladislav.reviewproject.api.dto;

import com.google.gson.annotations.SerializedName;

public class AppointmentDto {
    @SerializedName("globalAppointmentId")
    public Long crpId;
    @SerializedName("misAppointmentId")
    public String moId;
    @SerializedName("slotId")
    public String slotId;
    @SerializedName("patientId")
    public String patientId;
    @SerializedName("reason")
    public String reason;
}
